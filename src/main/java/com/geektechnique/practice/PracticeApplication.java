package com.geektechnique.practice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
@EnableConfigurationProperties
public class PracticeApplication {

    public static void main(String[] args) {


//        ConfigurableApplicationContext ctx = SpringApplication.run(PracticeApplication.class, args);
//        PracticeApplication myApp = (PracticeApplication) ctx.getBean("practiceApplication");
//        System.out.println(myApp.toString());

        SpringApplication.run(PracticeApplication.class, args);
    }

}
