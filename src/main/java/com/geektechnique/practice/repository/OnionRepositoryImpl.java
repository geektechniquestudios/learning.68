package com.geektechnique.practice.repository;

import com.geektechnique.practice.config.Onion;
import org.apache.catalina.User;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public class OnionRepositoryImpl implements OnionRepository {

    private RedisTemplate<String, Onion> redisTemplate;

    private HashOperations hashOperations;

    public OnionRepositoryImpl(RedisTemplate<String, Onion> redisTemplate){
        this.redisTemplate = redisTemplate;

        hashOperations = redisTemplate.opsForHash();
    }

    @Override
    public void save(Onion onionConfig) {

        hashOperations.put("Onion", onionConfig.getOnionId(), onionConfig);

    }

    @Override
    public Map<String, Onion> findAll() {
        return null;
    }

    @Override
    public Onion findById(String id) {
        return null;
    }

    @Override
    public void update(Onion onionConfig) {

    }

    @Override
    public void delete(String id) {

    }
}
