package com.geektechnique.practice.repository;

import com.geektechnique.practice.config.Onion;

import java.util.Map;

public interface OnionRepository {
    void save(Onion onionConfig);
    Map<String, Onion> findAll();
    Onion findById(String id);
    void update(Onion onionConfig);
    void delete(String id);
}
