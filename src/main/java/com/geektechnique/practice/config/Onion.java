package com.geektechnique.practice.config;


import lombok.*;
import org.springframework.stereotype.Component;

@Component
//@ConditionalOnClass(DummyClass.class)

@Data
@NoArgsConstructor
@AllArgsConstructor

//@ConfigurationProperties(prefix = "onion.config")
public class Onion {

    private String onionName;
    private String onionGarden;
    private int onionId;

}
