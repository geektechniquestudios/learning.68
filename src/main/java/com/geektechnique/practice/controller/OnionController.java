package com.geektechnique.practice.controller;

import com.geektechnique.practice.condition.DummyClass;
import com.geektechnique.practice.config.Onion;
import com.geektechnique.practice.repository.OnionRepository;
import org.springframework.context.annotation.Conditional;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Conditional(DummyClass.class)
@Controller
public class OnionController {

    private OnionRepository onionRepository;

    //@Autowired //Do I actually need to autowire
    public OnionController(OnionRepository onionRepository) { this.onionRepository = onionRepository;
    }

    @GetMapping("/onions")//no db integration, just thymeleaf
    public String showOnion(
            @RequestParam(name="which-onion", required = false, defaultValue = "nothing entered") String param, Model model){
        model.addAttribute("onion", param);


        return "onionPage";
    }

    @GetMapping("/add/{id}/{name}/{garden}")
    public String add(@PathVariable("id") final int id,
                     @PathVariable("name") final String name,
                     @PathVariable("garden") final String garden){
        onionRepository.update(new Onion(name, garden, id));
        return "addOnionPage";
    }

    //@Conditional(DummyClass.class) //doesn't work, will check my other work.
    @RequestMapping("/all")//figure out how to use the repo and send to thymeleaf
    public Map<String, Onion> allOnions(){
        return onionRepository.findAll();
    }

//    @RequestMapping("/hello")//can only get to work with restcontroller
//    public String hello(@RequestParam String name, Principal user){
//        return "Test hello " + user.getName(); //gets name based who is logged in
//    }



}
